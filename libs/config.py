import os

from dotenv import load_dotenv


class Config:
    load_dotenv()
    URL = os.environ.get("URL")
    TOKEN = os.environ.get("TOKEN")
    CLIENT_ID = os.environ.get("CLIENT_ID")
    ALBUM_ID = os.environ.get("ALBUM_ID")
    TOKEN2 = os.environ.get("TOKEN2")
    TOKENALBUM = os.environ.get("TOKENALBUM")
    TRACK_ID = os.environ.get("TRACK_ID")

