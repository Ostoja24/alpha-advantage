import json
import pytest
from assertpy import assert_that, soft_assertions
from datetime import datetime
import pytz
from zoneinfo import ZoneInfo
from datetime import timezone

@pytest.mark.test
class TestIndicators:
    def test_01_gdp(self, config, read):
        r = read(f"{config.URL}function=REAL_GDP&apikey={config.TOKEN}", headers=None,
                 auth=None,
                 json=None,
                 timeout=3)
        nr = r.json()
        # for i in range(len(nr['data']['date'])):
        #     date = date
        # print(nr['data']['date'])

        with soft_assertions():
            assert_that(r.status_code).is_equal_to(200)
            assert_that(nr['name']).is_equal_to("Real Gross Domestic Product")

            # assert_that(nr['data'](0)['date'](0)).does_not_contain("2022")
            # assert_that(enumerate(r.json()['data']))
            # assert_that(r.json()['data']['value']).contains(".")

            # r.json()['name'].is_equal_to("Real Gross Domestic Product")

    def test_02_ty(self, config, read):
        r = read(f"{config.URL}function=TREASURY_YIELD&apikey={config.TOKEN}", headers=None,
                 auth=None,
                 json=None,
                 timeout=3)
        nr = r.json()
        today = datetime.now()
        # datanr = nr['data'][i]['date']
        # date_time_obj_treasurybonds = datetime.strptime(datanr, '%Y-%m-%d')
        list_lenght = len(r.json()['data'])
        # value = nr['data'][0]['value']
        # valuefloat = float(value)
        with soft_assertions():
            assert_that(r.status_code).is_equal_to(200)
            assert_that(nr['name']).is_equal_to("10-Year Treasury Constant Maturity Rate")
            for i in range(list_lenght):
                assert_that(datetime.strptime(r.json()['data'][i]['date'], '%Y-%m-%d')).is_before(today)
                assert_that(float(r.json()['data'][i]['value'])).is_between(-99.99 , 99.99)
                assert_that(float(r.json()['data'][i]['value'])).is_not_equal_to(-100.00)
                assert_that(float(r.json()['data'][i]['value'])).is_not_equal_to(100.00)


    def test_03_inflation(self, config, read):
        r = read(f"{config.URL}function=INFLATION_EXPECTATION&apikey={config.TOKEN}", headers=None,
                 auth=None,
                 json=None,
                 timeout=3)
        nr = r.json()
        with soft_assertions():
            assert_that(r.status_code).is_equal_to(200)
            assert_that(nr['name']).is_equal_to("Inflation Expectations")

    def test_04_company_overview(self, config, read):
        r = read(f"{config.URL}function=OVERVIEW&symbol=IBM&apikey={config.TOKEN}", headers=None,
                 auth=None,
                 json=None,
                 timeout=3)
        nr = r.json()
        today = datetime.now()
        date_time_str_div = nr['DividendDate']
        date_time_obj_div = datetime.strptime(date_time_str_div, '%Y-%m-%d')
        date_time_str_exdiv = nr['ExDividendDate']
        date_time_obj_exdiv = datetime.strptime(date_time_str_exdiv, '%Y-%m-%d')
        with soft_assertions():
            assert_that(r.status_code).is_equal_to(200)
            assert_that(nr['Symbol']).is_equal_to("IBM")
            assert_that(date_time_obj_div).is_after(today)
            assert_that(date_time_obj_exdiv).is_before(today)

    def test_05_currency_exchange_rate(self,config,read):
        r = read(f"{config.URL}function=CURRENCY_EXCHANGE_RATE&from_currency=PLN&to_currency=USD&apikey={config.TOKEN}", headers=None,
                 auth=None,
                 json=None,
                 timeout=3)
        nr = r.json()
        realtimeexchange = nr['Realtime Currency Exchange Rate']
        realtimeexchangerate = float(realtimeexchange["5. Exchange Rate"])
        my_date = datetime.now(pytz.timezone('UTC'))
        realtimedatetime = realtimeexchange["6. Last Refreshed"]
        realtimedatetime_obj = datetime.strptime(realtimedatetime, '%Y-%m-%d %H:%M:%S')
        realtimedatetime_obj = realtimedatetime_obj.replace(tzinfo=timezone.utc)
        realtimeexchangebidprice = float(realtimeexchange["8. Bid Price"])
        realtimeexchangeaskprice = float(realtimeexchange["9. Ask Price"])
        print(realtimeexchange)
        with soft_assertions():
            assert_that(r.status_code).is_equal_to(200)
            assert_that(realtimeexchange["1. From_Currency Code"]).is_equal_to("PLN")
            assert_that(realtimeexchange["2. From_Currency Name"]).is_equal_to("Polish Zloty")
            assert_that(realtimeexchange["3. To_Currency Code"]).is_equal_to("USD")
            assert_that(realtimeexchange["4. To_Currency Name"]).is_equal_to("United States Dollar")
            assert_that(realtimeexchangerate).is_positive()
            assert_that(realtimeexchangebidprice).is_positive()
            assert_that(realtimeexchangeaskprice).is_positive()
            assert_that(realtimedatetime_obj).is_before(my_date)
    def test_06_GDPpercapita(self,config,read):
        r = read(f"{config.URL}function=REAL_GDP_PER_CAPITA&apikey={config.TOKEN}",
                 headers=None,
                 auth=None,
                 json=None,
                 timeout=5)
        nr = r.json()
        list_lenght = len(r.json()['data'])
        today = datetime.now()
        with soft_assertions():
            assert_that(r.status_code).is_equal_to(200)
            assert_that(nr['name']).is_equal_to("Real Gross Domestic Product per Capita")
            for i in range(list_lenght):
                assert_that(datetime.strptime(r.json()['data'][i]['date'], '%Y-%m-%d')).is_before(today)
                assert_that(abs((datetime.strptime(r.json()['data'][i]['date'], '%Y-%m-%d'))-(datetime.strptime(r.json()['data'][i-1]['date'], '%Y-%m-%d'))) == (timedelta(weeks=12)))
                assert_that(float(nr['data'][i]['value'])).is_positive()

    def test_07_dcm(self,config,read):
        r = read(f"{config.URL}function=DIGITAL_CURRENCY_MONTHLY&symbol=BTC&market=PLN&apikey={config.TOKEN}",
                 headers=None,
                 auth=None,
                 json=None,
                 timeout=5)
        nr = r.json()
        list_lenght_date = len(r.json()["Time Series (Digital Currency Monthly)"])
        list_lenght_values = len(r.json()["Time Series (Digital Currency Monthly)"]["2021-11-30"])
        print(list_lenght_date)
        print(list_lenght_values)
        today = datetime.now()
        with soft_assertions():
            assert_that(r.status_code).is_equal_to(200)
            assert_that(nr["Meta Data"]["1. Information"]).is_equal_to("Monthly Prices and Volumes for Digital Currency")
            for i in range(list_lenght_date):
                for y in range(list_lenght_values):
                    assert_that(float(r.json()["Time Series (Digital Currency Monthly)"][i][y])).is_positive()
                    assert_that(len(r.json()["Time Series (Digital Currency Monthly)"][i][y].split("."))).is_equal_to(8)








